package com.sapient.employee.service.impl;

import com.sapient.employee.constants.ErrorCode;
import com.sapient.employee.dto.EmployeeResponse;
import com.sapient.employee.dto.Range;
import com.sapient.employee.entity.Employee;
import com.sapient.employee.exception.EmployeeRuntimeException;
import com.sapient.employee.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.*;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class EmployeeServiceImplTest {

    @Mock
    EmployeeRepository employeeRepository;

    @InjectMocks
    EmployeeServiceImpl employeeService;

    PageImpl<Employee> employeePage;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        Employee employee = new Employee();
        employee.setId(21L);
        employee.setSalary(new BigDecimal("10000"));
        employee.setCompetency("Developer");
        employee.setName("Sumesh");
        employee.setPlace("Bangalore");
        employee.setBusinessUnit("BFS");
        employee.setTitle("Developer");
        employee.setSupervisorId(11L);

        Employee employee1 = new Employee();
        employee1.setId(22L);
        employee1.setSalary(new BigDecimal("25000"));
        employee1.setCompetency("Developer");
        employee1.setName("Rajesh");
        employee1.setPlace("Bangalore");
        employee1.setBusinessUnit("BFS");
        employee1.setTitle("Developer");
        employee1.setSupervisorId(11L);
        employeePage = new PageImpl<>(Arrays.asList(employee, employee1));
        when(employeeRepository.findByPlace(anyString(), any())).thenReturn(employeePage);
    }
    @Test
    void getEmployeesByPlace() {
        EmployeeResponse employeesByPlace = employeeService.getEmployeesByPlace("Bangalore", PageRequest.of(0, 5, Sort.by("id")));

        assertEquals("Bangalore", employeesByPlace.getEmployees().get(0).getPlace());
        assertEquals("Sumesh", employeesByPlace.getEmployees().get(0).getName());
        assertEquals("Developer", employeesByPlace.getEmployees().get(0).getTitle());
        assertEquals("10000", employeesByPlace.getEmployees().get(0).getSalary().toPlainString());
    }

    @Test
    void getEmployeesByPlace_exception() {
        when(employeeRepository.findByPlace(anyString(), any())).thenReturn(Page.empty());
        try {
            employeeService.getEmployeesByPlace("Bangalore", Pageable.unpaged());
        } catch (EmployeeRuntimeException ex) {
            assertEquals(ErrorCode.NOT_FOUND, ex.getErrorCode());
            assertEquals("Employee details not found for the given place", ex.getMessage());
        }
    }

    @Test
    void updateEmployeeSalary() {
        Iterable<Employee> employeeSalary = employeeService.updateEmployeeSalary("Bangalore", 20);
        assertEquals("12000", employeeSalary.iterator().next().getSalary().toPlainString());

    }

    @Test
    void getSalaryRange() {
        when(employeeRepository.findByCompetency(anyString())).thenReturn(employeePage.getContent());
        Range salaryRange = employeeService.getSalaryRange("Developer");
        assertEquals("10000", salaryRange.getRangeFrom().toString());
        assertEquals("25000", salaryRange.getRangeTo().toString());

    }
}