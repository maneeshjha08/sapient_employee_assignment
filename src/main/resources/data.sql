DROP TABLE IF EXISTS employee;

CREATE TABLE EMPLOYEE (
  EMPLOYEE_ID INT AUTO_INCREMENT  PRIMARY KEY,
  EMPLOYEE_NAME VARCHAR(250) NOT NULL,
  TITLE VARCHAR(250) NOT NULL,
  BUSINESS_UNIT VARCHAR(250) DEFAULT NULL,
  PLACE VARCHAR(250) DEFAULT NULL,
  SUPERVISOR_ID VARCHAR(250) DEFAULT NULL,
  COMPETENCY VARCHAR(250) DEFAULT NULL,
  SALARY VARCHAR(250) DEFAULT NULL
);

INSERT INTO EMPLOYEE (EMPLOYEE_NAME, TITLE, BUSINESS_UNIT, PLACE, SUPERVISOR_ID, COMPETENCY, SALARY)
VALUES
('Rajesh', 'CEO', 'BFS', 'Bangalore', '1', 'Head', '7000000');
INSERT INTO EMPLOYEE (EMPLOYEE_NAME, TITLE, BUSINESS_UNIT, PLACE, SUPERVISOR_ID, COMPETENCY, SALARY)
VALUES
('Sumesh', 'Developer', 'BFS', 'Bangalore', '1', 'Developer', '200000');
INSERT INTO EMPLOYEE (EMPLOYEE_NAME, TITLE, BUSINESS_UNIT, PLACE, SUPERVISOR_ID, COMPETENCY, SALARY)
VALUES
('Kamal', 'Developer', 'BFS', 'Bangalore', '1', 'Developer', '150000');
INSERT INTO EMPLOYEE (EMPLOYEE_NAME, TITLE, BUSINESS_UNIT, PLACE, SUPERVISOR_ID, COMPETENCY, SALARY)
VALUES
('Srini', 'Developer', 'BFS', 'Bangalore', '1', 'Developer', '120000');
INSERT INTO EMPLOYEE (EMPLOYEE_NAME, TITLE, BUSINESS_UNIT, PLACE, SUPERVISOR_ID, COMPETENCY, SALARY)
VALUES
('Preeti', 'Developer', 'BFS', 'Bangalore', '1', 'Developer', '400000');

commit;