package com.sapient.employee.service;

import com.sapient.employee.dto.EmployeeResponse;
import com.sapient.employee.dto.Range;
import com.sapient.employee.entity.Employee;
import org.springframework.data.domain.Pageable;

public interface EmployeeService {

    EmployeeResponse getEmployeesByPlace(String place, Pageable pageable);

    Iterable<Employee> updateEmployeeSalary(String place, Integer salaryPercentage);

    Range getSalaryRange(String competency);

}
