package com.sapient.employee.service.impl;

import com.sapient.employee.constants.ErrorCode;
import com.sapient.employee.dto.EmployeeResponse;
import com.sapient.employee.dto.Range;
import com.sapient.employee.entity.Employee;
import com.sapient.employee.exception.EmployeeRuntimeException;
import com.sapient.employee.helper.EmployeeHelper;
import com.sapient.employee.repository.EmployeeRepository;
import com.sapient.employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    static final int SALARY_PERCENTAGE_THRESHOLD = 55;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public EmployeeResponse getEmployeesByPlace(String place, Pageable pageable) {
        List<Employee> employees = getEmployees(place, pageable);
        return EmployeeHelper.mapEmployeeResponse(employees);
    }

    @Override
    public List<Employee> updateEmployeeSalary(String place, Integer salaryPercentage) {
        if (salaryPercentage > SALARY_PERCENTAGE_THRESHOLD) {
            throw new EmployeeRuntimeException("Salary percentage exceeded.", ErrorCode.VALIDATION_FAILED);
        }
        List<Employee> employees = getEmployees(place, Pageable.unpaged());
        employees.forEach(employee -> employee.setSalary(getRevisedSalary(salaryPercentage, employee)));
        employeeRepository.saveAll(employees);
        return employees;
    }

    @Override
    public Range getSalaryRange(String competency) {
        List<Employee> employees = employeeRepository.findByCompetency(competency);
        if (CollectionUtils.isEmpty(employees)) {
            throw new EmployeeRuntimeException("Employees not found for the given competency.", ErrorCode.NOT_FOUND);
        }
        List<BigDecimal> salaries = employees.stream().map(Employee::getSalary).collect(Collectors.toList());
        LongSummaryStatistics salaryRange = getRange(salaries);
        if (salaryRange != null) {
            Range range = new Range();
            range.setRangeFrom(salaryRange.getMin());
            range.setRangeTo(salaryRange.getMax());
            return range;
        }
        throw new EmployeeRuntimeException("Salary range not found.", ErrorCode.NOT_FOUND);
    }

    private LongSummaryStatistics getRange(Collection<BigDecimal> collection) {
        return Optional.of(collection)
                .filter(c -> !c.isEmpty())
                .map(Collection::stream)
                .map(s -> s.mapToLong(BigDecimal::longValue))
                .map(LongStream::summaryStatistics)
                .orElse(null);
    }

    private BigDecimal getRevisedSalary(Integer salaryPercentage, Employee employee) {
        return employee.getSalary().add(BigDecimal.valueOf((employee.getSalary().intValue() / 100) * salaryPercentage));
    }

    private List<Employee> getEmployees(String place, Pageable pageable) {
        Page<Employee> employees = employeeRepository.findByPlace(place, pageable);
        if (employees.isEmpty()) {
            throw new EmployeeRuntimeException("Employee details not found for the given place", ErrorCode.NOT_FOUND);
        }
        return employees.getContent();
    }
}
