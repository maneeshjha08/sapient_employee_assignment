package com.sapient.employee.controller;

import com.sapient.employee.dto.RestResponse;
import com.sapient.employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping("/place/{place}")
    public ResponseEntity<RestResponse> getEmployees(@PathVariable(name = "place") String place, @RequestParam(defaultValue = "0") Integer pageNo,
                                                     @RequestParam(defaultValue = "5") Integer pageSize,
                                                     @RequestParam(defaultValue = "id") String sortBy) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        return new ResponseEntity<>(new RestResponse(Boolean.TRUE,
                "Employee details fetched successfully",
                employeeService.getEmployeesByPlace(place, pageable)),
                HttpStatus.OK);
    }

    @PutMapping("/place/{place}/salary/{percentage}")
    public ResponseEntity<RestResponse> updateEmployeeSalary(@PathVariable(name = "place") String place,
                                                             @PathVariable(name = "percentage") Integer salaryPercenatge) {
        employeeService.updateEmployeeSalary(place, salaryPercenatge);
        return new ResponseEntity<>(
                new RestResponse(Boolean.TRUE,
                        "Employee updated successfully",
                        null),
                HttpStatus.CREATED);
    }

    @GetMapping("/competency/{competency}")
    public ResponseEntity<RestResponse> getSalaryRange(@PathVariable(name = "competency") String competency) {
        return new ResponseEntity<>(new RestResponse(Boolean.TRUE,
                "Employee details fetched successfully",
                employeeService.getSalaryRange(competency)),
                HttpStatus.OK);
    }
}
