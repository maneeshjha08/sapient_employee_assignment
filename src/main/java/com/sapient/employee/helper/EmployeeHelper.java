package com.sapient.employee.helper;

import com.sapient.employee.dto.EmployeeDto;
import com.sapient.employee.dto.EmployeeResponse;
import com.sapient.employee.entity.Employee;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeHelper {

    public static EmployeeResponse mapEmployeeResponse(List<Employee> employeeList) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        List<EmployeeDto> employeeDtos = employeeList.stream()
                .map(employee -> (EmployeeDto) copyProperties(employee, new EmployeeDto()))
                .collect(Collectors.toList());
        employeeResponse.setEmployees(employeeDtos);
        return employeeResponse;
    }

    private static Object copyProperties(Object source, Object target) {
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
