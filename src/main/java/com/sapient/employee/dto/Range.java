package com.sapient.employee.dto;

import lombok.Data;

@Data
public class Range {

    Object rangeFrom;

    Object rangeTo;
}
