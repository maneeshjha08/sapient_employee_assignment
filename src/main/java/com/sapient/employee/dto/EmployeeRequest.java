package com.sapient.employee.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class EmployeeRequest {

    private long employeeId;

    private String employeeName;

    private String place;

    private String title;

    private long supervisorId;

    private String businessUnit;

    private String competency;
}
