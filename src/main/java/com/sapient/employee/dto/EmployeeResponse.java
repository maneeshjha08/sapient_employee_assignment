package com.sapient.employee.dto;

import com.sapient.employee.entity.Employee;
import lombok.Data;

import java.util.List;

@Data
public class EmployeeResponse {

    private List<EmployeeDto> employees;
}
