package com.sapient.employee.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class EmployeeDto {

    private long id;

    private String name;

    private String title;

    private String businessUnit;

    private String place;

    private long supervisorId;

    private String competency;

    private BigDecimal salary;

}
