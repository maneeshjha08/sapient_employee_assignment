package com.sapient.employee.dto;

import com.sapient.employee.constants.ErrorCode;
import lombok.Data;

import java.util.List;

@Data
public class RestResponse {

    private boolean success;

    private String message;

    private Object object;

    private String errorCode;

    public RestResponse(boolean success, String message, Object object, String errorCode) {
        this.success = success;
        this.message = message;
        this.object = object;
        this.errorCode = errorCode;
    }
    public RestResponse(boolean success, String message, Object object) {
        this.success = success;
        this.message = message;
        this.object = object;
    }

    public RestResponse( String message, String errorCode) {
        this.message = message;
        this.errorCode = errorCode;
    }
}
