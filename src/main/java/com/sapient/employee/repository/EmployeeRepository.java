package com.sapient.employee.repository;

import com.sapient.employee.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long> {

    @Query("select emp from Employee emp where upper(emp.place) = upper(:place)")
    Page<Employee> findByPlace(@Param("place") String place, Pageable pageable);

    List<Employee> findByCompetency(String competency);
}
