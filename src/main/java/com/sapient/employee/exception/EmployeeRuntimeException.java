package com.sapient.employee.exception;

import com.sapient.employee.constants.ErrorCode;
import lombok.Getter;

public class EmployeeRuntimeException extends RuntimeException {

    @Getter
    private final ErrorCode errorCode;

    public EmployeeRuntimeException(String message, ErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public EmployeeRuntimeException(String message, ErrorCode errorCode, Throwable throwable) {
        super(message, throwable);
        this.errorCode = errorCode;
    }
}
