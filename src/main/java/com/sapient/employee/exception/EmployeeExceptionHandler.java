package com.sapient.employee.exception;

import com.sapient.employee.constants.ErrorCode;
import com.sapient.employee.dto.RestResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class EmployeeExceptionHandler {

    @ExceptionHandler(EmployeeRuntimeException.class)
    public ResponseEntity<?> handleEmployeeRuntimeException(EmployeeRuntimeException ex) {
        return buildResponseEntity(new RestResponse(ex.getMessage(), ex.getErrorCode().name()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception ex) {
        return buildResponseEntity(new RestResponse(ex.getMessage(), ErrorCode.INTERNAL_SERVER_ERROR.name()));
    }

    private ResponseEntity<?> buildResponseEntity(RestResponse restResponse) {
        HttpStatus httpStatus;

        ErrorCode errorCode = ErrorCode.valueOf(restResponse.getErrorCode());
        switch (errorCode) {
            case VALIDATION_FAILED:
            case SYSTEM_ERROR:
                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            case NOT_FOUND:
                httpStatus = HttpStatus.NOT_FOUND;
            default:
                httpStatus = HttpStatus.OK;
        }
        return new ResponseEntity<>(restResponse, httpStatus);
    }
}
