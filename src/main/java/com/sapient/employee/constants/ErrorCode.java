package com.sapient.employee.constants;

public enum ErrorCode {

    SYSTEM_ERROR,
    NOT_FOUND,
    VALIDATION_FAILED,
    INTERNAL_SERVER_ERROR
}
